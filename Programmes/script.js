var sizeFactor = 0.98;
var container; 

var camera, scene, renderer, light;
var dax=0.002;
var wall = [];

// PLAYER STATS
var playerMovementSpeed = 0.035;
var rotationSpeed = 0.02;
var playerHeight = 1.68; // Taille de Mich
var gravity = 0.0040;
var speedZ = 0;

// MOVE VARIABLE
var rotateLeft = false;
var rotateRight = false;
var rotateUp = false;
var rotateDown = false;
var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var resetRotation = false;
var distanceToTexture = 1.75;
const boundaryXm = -6 + distanceToTexture;
const boundaryXp = 6 - distanceToTexture;
const boundaryYm = -11.5 + distanceToTexture;
const boundaryYp = 11.5 - distanceToTexture;
const boundaryZm = -22;
const boundaryZp = 22 - distanceToTexture;

// INDICES
var mesh1,mesh2,mesh3,mesh4,mesh5;
var text1,text2,text3,text4,text5,text6;
var tableau = false;
var coin = false;
var autel = false;
var solution = false;
var end = false;

// Debug vars
var debug = true;
var coordsText;


init();

function init() {

    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 2000 );
    camera.castShadow = false;
    camera.rotation.z=0;
    camera.rotation.x=(90 * Math.PI / 180);
    camera.rotation.y=0;
    camera.position.x=0;
    camera.position.y=0;
    camera.position.z= -20.40;

    // scene
    scene = new THREE.Scene();

    var ambient = new THREE.AmbientLight( 0x2f4f4f );
    //var ambient = new THREE.AmbientLight( 0xFFFFFF );
    scene.add( ambient );

    light = new THREE.PointLight(0xff4400, 5, 10, 2);
    light.position.copy(camera.position);
    scene.add(light);    

    //Indice 1

    var GCube1 = new THREE.BoxGeometry(0.5, 0.5, 0.5)
    var MCube1 = new THREE.MeshPhongMaterial({ambient: 0x000000,
    color : 0xFF0000, opacity: 0.35,specular:0x555555,shininess:20, transparent: true });

    mesh1 = new THREE.Mesh(GCube1, MCube1);

    mesh1.position.x=6;
    mesh1.position.y=-1.75;
    mesh1.position.z=-20.5;
    scene.add( mesh1 );

    var GCube2 = new THREE.BoxGeometry(0.5, 0.5, 0.5)
    var MCube2 = new THREE.MeshPhongMaterial({ambient: 0x000000,
    color : 0x00FF00,opacity: 0.15,specular:0x555555,shininess:20, transparent: true });

    mesh2 = new THREE.Mesh(GCube2, MCube2);
    mesh2.position.x=0;
    mesh2.position.y=-11.5;
    mesh2.position.z=-50;

    mesh3 = new THREE.Mesh(GCube2, MCube2);
    mesh3.position.x=-5.5;
    mesh3.position.y=11;
    mesh3.position.z=-50;

    
    mesh4 = new THREE.Mesh(GCube2, MCube2);
    mesh4.position.x=-6;
    mesh4.position.y=-2.65;
    mesh4.position.z=-50;

    text1 = document.createElement('div');
    text1.style.position = 'absolute';
    text1.style.backgroundColor = "#00000069";
    text1.innerHTML = "Bonjour à toi chèr pèlerin, afin d'éveiller ton esprit, aide-moi à trouver<br/><br/>- De l'eau bénite<br/>- Une coupe<br/>- Un crucifix ";//changer le texte
    text1.style.color = "gold";
    text1.style.textAlign = "center";
    text1.style.fontSize = 30 + "px"
    text1.style.top = "35%";
    text1.style.bottom = "35%";
    text1.style.right = "35%";
    text1.style.left = "35%";
    document.body.appendChild(text1);

    text2 = document.createElement('div');
    text2.style.position = 'absolute';
    text2.style.backgroundColor = "#00000069";
    text2.innerHTML = "<br/>Objet 2<br/><br/>Vous trouvez de l'eau bénite posée sur l'autel";
    text2.style.color = "gold";
    text2.style.textAlign = "center";
    text2.style.fontSize = 30 + "px"
    text2.style.top = "35%";
    text2.style.bottom = "35%";
    text2.style.right = "35%";
    text2.style.left = "35%";
    document.body.appendChild(text2);

    text3 = document.createElement('div');
    text3.style.position = 'absolute';
    text3.style.backgroundColor = "#00000069";
    text3.innerHTML = "<br/>Objet 3<br/><br/>Vous venez de trouver une coupe posée sur le sol";
    text3.style.color = "gold";
    text3.style.textAlign = "center";
    text3.style.fontSize = 30 + "px"
    text3.style.top = "35%";
    text3.style.bottom = "35%";
    text3.style.right = "35%";
    text3.style.left = "35%";
    document.body.appendChild(text3);

    text4 = document.createElement('div');
    text4.style.position = 'absolute';
    text4.style.backgroundColor = "#00000069";
    text4.innerHTML = "<br/>Objet 1<br/><br/>Vous arrachez un bout de toile, et découvrez un crucifix";
    text4.style.color = "gold";
    text4.style.textAlign = "center";
    text4.style.fontSize = 30 + "px"
    text4.style.top = "35%";
    text4.style.bottom = "35%";
    text4.style.right = "35%";
    text4.style.left = "35%";
    document.body.appendChild(text4);

    text5 = document.createElement('div');
    text5.style.position = 'absolute';
    text5.style.backgroundColor = "#00000069";
    text5.innerHTML = "<br/>Je vous remercie!<br/><br/> Rendez-vous près de l'autel, et rejoignez notre seigneur";//changer le texte
    text5.style.color = "gold";
    text5.style.textAlign = "center";
    text5.style.fontSize = 30 + "px"
    text5.style.top = "35%";
    text5.style.bottom = "35%";
    text5.style.right = "35%";
    text5.style.left = "35%";
    document.body.appendChild(text5);

    coordsText = document.createElement('div');
    coordsText.style.position = 'absolute';
    coordsText.style.backgroundColor = "#00000069";
    coordsText.style.color = "white";
    coordsText.style.textAlign = "left";
    coordsText.style.top = "2%";
    coordsText.style.left = "2%";
    coordsText.style.width = "auto";
    coordsText.style.height = "auto";
    document.body.appendChild(coordsText);

    var GCube3 = new THREE.BoxGeometry(0.5, 0.5, 0.5)
    var MCube3 = new THREE.MeshPhongMaterial({ambient: 0x000000,
    color : 0x0000FF, opacity: 0.15,shininess:20, transparent: true });

    mesh5 = new THREE.Mesh(GCube3, MCube3);

    mesh5.position.x=0;
    mesh5.position.y=-11.5;
    mesh5.position.z=-6;


    text6 = document.createElement('div');
    text6.style.position = 'absolute';
    text6.style.backgroundColor = "#00000069";
    text6.innerHTML = "<br/>Vous venez d'atteindre l'illumination!!! <br/><br/>Félicitation!";
    text6.style.color = "gold";
    text6.style.textAlign = "center";
    text6.style.fontSize = 30 +"px"
    text6.style.top = "35%";
    text6.style.bottom = "35%";
    text6.style.right = "35%";
    text6.style.left = "35%";
    document.body.appendChild(text6);
    
    // texture

    var manager = new THREE.LoadingManager();
    manager.onProgress = function ( item, loaded, total ) {
        console.log( item, loaded, total );
    };

    var textureLoader = new THREE.TextureLoader();

    // plans

    wall.front = new THREE.Mesh( new THREE.PlaneGeometry(12, 44), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/front.png", function(){Afficher();}),
    }) );
    wall.front.position.set(0, 11.5, 0);
    wall.front.rotateX(Math.PI / 2);
    scene.add(wall.front);

    wall.back = new THREE.Mesh( new THREE.PlaneGeometry(12, 44), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/back.png", function(){Afficher();}),
    }) );
    wall.back.position.set(0, -11.5, 0);
    wall.back.rotateX(Math.PI / 2);
    wall.back.rotateY(Math.PI);
    scene.add(wall.back);

    wall.left = new THREE.Mesh( new THREE.PlaneGeometry(23, 44), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/left.png", function(){Afficher();}),
    }) );
    wall.left.position.set(-6, 0, 0);
    wall.left.rotateX(Math.PI / 2);
    wall.left.rotateY(Math.PI / 2);
    scene.add(wall.left);

    wall.right = new THREE.Mesh( new THREE.PlaneGeometry(23, 44), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/right.png", function(){Afficher();}),
    }) );
    wall.right.position.set(6, 0, 0);
    wall.right.rotateX(Math.PI / 2);
    wall.right.rotateY(3 * Math.PI / 2);
    scene.add(wall.right);

    wall.floor = new THREE.Mesh( new THREE.PlaneGeometry(12, 23), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/floor.png", function(){Afficher();}),
    }) );
    wall.floor.position.set(0, 0, -22);
    scene.add(wall.floor);

    wall.roof = new THREE.Mesh( new THREE.PlaneGeometry(12, 44), new THREE.MeshPhysicalMaterial({
        map: textureLoader.load("Textures/roof.png", function(){Afficher();}),
    }) );
    wall.roof.position.set(0, 11.5, 22);
    wall.roof.rotateX(Math.PI);
    scene.add(wall.roof);



    renderer = new THREE.WebGLRenderer({
        antialias: true,
    });
    renderer.setSize( window.innerWidth * sizeFactor, window.innerHeight * sizeFactor );
    renderer.setAnimationLoop(animation);
	document.body.appendChild( renderer.domElement );
    window.addEventListener( 'resize', onWindowResize, false );
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 81) moveLeft = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 68) moveRight = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 90) moveForward = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 83) moveBackward = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 37) rotateLeft = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 39) rotateRight = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 38) rotateUp = true;}, false);
    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 40) rotateDown = true;}, false);

    window.addEventListener( 'keydown', function(ev) {if (ev.keyCode == 16) resetRotation = true;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 16) resetRotation = false;}, false);

    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 81) moveLeft = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 68) moveRight = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 90) moveForward = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 83) moveBackward = false;}, false);
  
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 32) speedZ = 0.12;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 37) rotateLeft = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 39) rotateRight = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 38) rotateUp = false;}, false);
    window.addEventListener( 'keyup', function(ev) {if (ev.keyCode == 40) rotateDown = false;}, false);

}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( sizeFactor * window.innerWidth, sizeFactor * window.innerHeight );
}

function Afficher() {
    camera.updateProjectionMatrix();
    renderer.render(scene,camera);
}

function animation( time ) {
    if (camera.position.z > (boundaryZm + playerHeight - speedZ)) { // Gravité
        speedZ -= gravity;
        camera.position.z += speedZ;
    } else if (camera.position.z > (boundaryZm + playerHeight)) {
        camera.position.z = boundaryZm + playerHeight;
    } else {
        speedZ = 0;
        camera.position.z = boundaryZm + playerHeight;
    }
    var previousRotation = camera.rotation.clone();
    if (rotateLeft && !rotateRight && !rotateUp && !rotateDown) { // Tourner la caméra vers la gauche
        camera.rotateOnAxis(new THREE.Vector3(0,1,0), rotationSpeed);
    }
    if (rotateRight && !rotateLeft && !rotateUp && !rotateDown) { // Tourner la caméra vers la droite
        camera.rotateOnAxis(new THREE.Vector3(0,-1,0), rotationSpeed);
    }
    if(rotateUp && !rotateDown && !rotateLeft && !rotateRight){ //Tourner la caméra vers le haut
        camera.rotateOnAxis(new THREE.Vector3(1,0,0), rotationSpeed);
    }
    if(rotateDown && !rotateLeft && !rotateRight && !rotateUp){ //Tourner la caméra vers le bas
        camera.rotateOnAxis(new THREE.Vector3(-1,0,0), rotationSpeed);
    }

    if (moveForward) { // Avancer la caméra
        camera.translateOnAxis(new THREE.Vector3(0,0,-1), playerMovementSpeed);
    }
    if (moveBackward) { // Reculer la caméra
        camera.translateOnAxis(new THREE.Vector3(0,0,1), playerMovementSpeed);
    }
    if(moveRight){ // Déplacer droite
        camera.translateOnAxis(new THREE.Vector3(1,0,0), playerMovementSpeed);
    }
    if(moveLeft){ //Déplacer gauche
        camera.translateOnAxis(new THREE.Vector3(-1,0,0), playerMovementSpeed);
    }
    if(resetRotation){
        camera.rotation.z=0;
        camera.rotation.x=90 * Math.PI / 180;
        camera.rotation.y=previousRotation.y;
    }

    if (camera.position.x < boundaryXm) {
        camera.position.x = boundaryXm;
    }
    if (camera.position.x > boundaryXp) {
        camera.position.x = boundaryXp;
    }
    if (camera.position.y < boundaryYm) {
        camera.position.y = boundaryYm;
    }
    if (camera.position.y > boundaryYp) {
        camera.position.y = boundaryYp;
    }
    if (camera.position.z < boundaryZm) {
        camera.position.z = boundaryZm;
    }
    if (camera.position.z > boundaryZp) {
        camera.position.z = boundaryZp;
    }

    if (coin && tableau && autel){
        solution = true;
    }
    //Indice Charlie début
    if(camera.position.distanceTo(mesh1.position)<3.5 && !solution){
        text1.style.display = "block";

        mesh2.position.z=-19.5;
        mesh3.position.z=-21.5;
        mesh4.position.z=-18.5;
        scene.add( mesh2 );
        scene.add( mesh3 );
        scene.add( mesh4 );

    }else{
        text1.style.display = "none";
    }

    //Indice Charlie Fin
    if(camera.position.distanceTo(mesh1.position)<3.5 && solution){
        text5.style.display = "block";
        scene.add( mesh5 );
        gravity = 0.0005;
        end = true;
    }else{
        text5.style.display = "none";
    }
    if(autel && camera.position.distanceTo(mesh2.position)>3.5){
        mesh2.position.z=-50;
    }
    if(coin && camera.position.distanceTo(mesh3.position)>3.5){
        mesh3.position.z=-50;
    }
    if(tableau && camera.position.distanceTo(mesh4.position)>3.5){
        mesh4.position.z=-50;
    }
    
    //Indice autel
    if(camera.position.distanceTo(mesh2.position)<3.5){
        text2.style.display = "block";
        scene.remove(mesh2);
        autel = true;
    }else{
        text2.style.display = "none";
    }

    //Indice coin
    if(camera.position.distanceTo(mesh3.position)<3.5){
        text3.style.display = "block";
        scene.remove(mesh3);
        coin = true;
    }else{
        text3.style.display = "none";
    }

    //Indice Tableau
    if(camera.position.distanceTo(mesh4.position)<3.5 ){
        text4.style.display = "block";            
        scene.remove(mesh4);
        tableau = true;
    }else{
        text4.style.display = "none";
    }
    //Fin
    if(camera.position.distanceTo(mesh5.position)<5 && end){
        text6.style.display = "block";
    }else{
        text6.style.display = "none";
    }

    //Coordonnées
    if(debug){
        coordsText.style.display = "block";
    }else{
        coordsText.style.display = "none";
    }

    coordsText.innerHTML = "x : " + camera.position.x.toFixed(2) + "<br/>y : " + camera.position.y.toFixed(2) + "<br/>z : " + camera.position.z.toFixed(2);
    light.position.copy(camera.position);

    Afficher();
}